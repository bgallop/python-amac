try:
    from i2c_rpi import i2c_rpi
except ImportError:
    print("not using raspberry pi")
try:    
    from i2c_hsio import i2c_hsio
except ImportError:
    print("not using hsio")
    
from register_map import amac_read_registers as read_register
from register_map import amac_write_registers as write_registers
from AmacSimulation import simulated_i2c
import parse_config as parse
import sys

def argstolist(args):
    adargs=list(args)
    ads=['']
    i=0
    for num in adargs:
        if (num != ':'):
            ads[i]+=num
        else:
            ads.append('')
            i+=1
    addresses=[int(i) for i in ads]
    return addresses

class amac:
    def __init__(self,address=0x01,channel=0,i2c='rpi'):
        global read_register
        global write_registers
        self.address=address
        
        if(i2c=='rpi'):
            self.i2c_comm=i2c_rpi(address,channel)
        elif(i2c=='hsio'):
            self.i2c_comm=i2c_hsio(address)
        elif(i2c=='sim'):
            print('simulating amac')
            self.i2c_comm=simulated_i2c()
        else:
            print('unimplemented i2c comm, choose between rpi and hsio')

    def __str__(self):
        return "amac address: "+str(self.address)

    def __repr__(self):
        return str(self)

    def write(self,register,value,offset):
        value=value << offset
        self.i2c_comm.write(register,value)

    def read(self,register,offset):
        value = self.i2c_comm.read(register)
        value = value & 0xff
        value = value >> offset
        return value
    
    def configure(self,config_file=None):
        #registers to configure (from Karol et al. group)
        if(config_file):
            try:
                port,address,register_data=parse.amac_config(config_file)
                register_data_map=parse.process_data(register_data)
                for reg,value in register_data_map.items():
                    self.i2c_comm.write(reg,value)
            except IOError:
                print("couldn't open config file")
                return
        else:      
            registers={
                'bandgap':0x01,
                'right_ch0_select':0x01,
                'left_ch0_select':0x01,
                'left_ch3_select':0,
                'right_ch3_select':0,
                'left_ramp_gain':0x03,
                'right_ramp_gain':0x03,
                'left_opamp_gain':0,
                'right_opamp_gain':0,
                'hv_freq':0x01}

            for name,data in registers.items():
                register=write_registers[name][0]
                offset=write_registers[name][1]
                self.write(register,data,offset)
    def lv_on(self):
        self.i2c_comm.write(44,1)

    def lv_off(self):
        self.i2c_comm.write(44,0)

    def hv_on(self):
        self.i2c_comm.write(43,1)

    def hv_off(self):
        self.i2c_comm.write(43,0)
        
    def temperature(self,side='left'):
        register_1,offset_1=read_register[side+'_temperature'][0]
        register_2,offset_2=read_register[side+'_temperature'][1]
        value_1=self.read(register_1,offset_1)
        value_2=self.read(register_2,offset_2)
        
        value_2 &=0x03
        value_2=value_2 << 8
        temperature=((value_1+value_2)*12.5-25)/1000
        #from line 1022 of amac1.cpp 
        return temperature

    def right_current(self):
        register_1,offset_1=read_register['right_current'][0]
        register_2,offset_2=read_register['right_current'][1]
        value_1=self.read(register_1,offset_1)
        value_2=self.read(register_2,offset_2)

        value_2=value_2 << 4
        value=value_1 |value_2
        gain=33400664.452 #taken from amac1.cpp, based on opamp gain settings
        current=(value - 106) * 1E6 / gain
        return current
    
    def left_current(self):
        register_1,offset_1=read_register['left_current'][0]
        register_2,offset_2=read_register['left_current'][1]
        value_1=self.read(register_1,offset_1)
        value_2=self.read(register_2,offset_2)

        value_2=value_2 << 4
        value=value_1 | value_2
        gain=33400664.452 #taken from amac1.cpp, based on opamp gain settings
        current=(value - 106) * 1E6 / gain
        return current

    #to get right vdd do amac.get_vdd('right')
    def get_vdd(self,side='left'):
        if(side != 'left' and side != 'right'):
            print("enter 'left' or 'right as arguement")
            return 0
        
        register_1,offset_1=read_register[side+'_vdd'][0]
        register_2,offset_2=read_register[side+'_vdd'][1]
        value_1=self.read(register_1,offset_1)
        value_2=self.read(register_2,offset_2)
        value_2=value_2 & 0x03
        value_2=value_2 << 8
        value=value_1 | value_2
        vdd= (1.25*value-25)*2 /1000
        return (value,vdd)

    def get_vchan(self,side='left'):
        register_1,offset_1=read_register[side+'_vdd'][0]
        register_2,offset_2=read_register[side+'_vdd'][1]
        value_1=self.read(register_1,offset_1)
        value_2=self.read(register_2,offset_2)
        value_1 &= 0x3f
        value_2 &=0x0f
        value_2 = value_2 << 6
        value =value_1 | value_2
        vdd= (1.25*value-25)*2 /1000
        return (value,vdd)

    def read_analogue(self):
        print("VDD_REG",self.get_vdd('left')) 
        print("VDD_RAW",self.get_vchan('left'))  
        print("Vbandgap") 
        print("VDD_HYBRID")
        print("Temperature",self.temperature('left'))
        print("OTA")
        print("I_Sensor ",self.left_current())
        print("V_IN",self.get_vdd('right'))
        print("I_OUT" )
        print("NTC")
        print("FEAST_PTAT")
        print("VDD_IO")
        print("OTA ")
        print("IchanRight",self.right_current())
        
    #data is 0 or 1 WIP need an |
    def ch0_select(self,side='left',data=0x01):
        register,offset=write_registers[side+"_ch0_select"]
        self.write(register,offset,data)


    
if __name__=="__main__":
    args=sys.argv
    if(':' in args[1]):
        addresses=argstolist(args[1])
    else:
        try:
            addresses=[int(args[1])]
        except:
            addresses=[0x01]
    
    stave_amacs=[amac(address=i,i2c='rpi') for i in addresses]
    
        
    print(stave_amacs)
    if(len(args)>2):
        if('config' in args):
            for amc in stave_amacs:
                amc.configure('example_config.det')
        if('on' in args):
            for amc in stave_amacs:
                amc.lv_on()
                amc.hv_on()        
        if('lvon' in args):
	    for amc in stave_amacs:
		amc.lv_on()
        if('hv_on' in args):
            for amc in stave_amacs:
                amc.hv_on()        
        elif('off' in args):
            for amc in stave_amacs:
                amc.lv_off()
                amc.hv_off()

        if('read' in args):
            for amc in stave_amacs:
                print("amacs number ",amc.address)
                amc.read_analogue()
                print("---------------------")
        for amc in stave_amacs:
            print("low voltage setting", amc.i2c_comm.read(44))
            print("high voltage setting", amc.i2c_comm.read(43))
    '''
    print("right")
    print("vdd reg",stave_amac.get_vdd('right'))
    print("vdd raw",stave_amac.get_vchan('right'))
    print("left")
    print("vdd reg",stave_amac.get_vdd('left'))
    print("vdd raw",stave_amac.get_vchan('left'))       
    '''
